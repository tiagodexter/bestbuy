First thing I did, was clone the remote repository on my local.
After that, I created my new repository on my CodeCommit resource, creating in this way, my first branch called master.
After that I created some files and also created my branch called develop, which will receive all development changes.
Files that I created:
- package.json
- .gitignore
- Dockerfile
- README.md
- buildspec.yml

The package.json contains the system information also scripts to start and test the application
On start script I putted the data about environment variables (ENV and PORT), so the system can start in the right environment.

The .gitignore I putted the folder node_modules and the package-lock.json, because, at this time, I don't want to lock my dependencies.

The Dockerfile I putted the configuration just to build a docker container image, and expose the same port that I putted on package.json file.

The buildspec.yml file is the file where I put all information about the build, using CodeBuild to build the image and push this image to ECR (Elastic Container Registry)
